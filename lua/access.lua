-- access.lua
local helper = require("helper").new()
local inspect = require("resty.inspect")

-- Ignore requests that are not POST
if ngx.req.get_method() ~= "POST" and ngx.req.get_method() ~= "PUT" then
    ngx.exit(ngx.OK)
end
-- Only handle jobs and plan requests.
if not ngx.var.uri:match("^/v1/jobs") and not ngx.var.uri:match("^/v1/job/.+/plan$") then
    ngx.exit(ngx.OK)
end

local token = helper:get_token() or {}
ngx.log(ngx.ERR, ("uses token %q %s"):format(token.Name, token.AccessorID))
local job = helper:get_job()
if not job then
    ngx.exit(ngx.OK)
end
if job.namespace == "block" then
    helper:exit_forbid("job in 'block' namespace is blocked")
end
ngx.exit(ngx.OK)
