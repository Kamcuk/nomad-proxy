-- balancer.lua
local httpc = require("resty.http")
local balancer = require("ngx.balancer")
local nomad_addr = os.getenv("NOMAD_ADDR")
if not nomad_addr then
    ngx.log(ngx.ERR, "NOMAD_ADDR variable not exported")
    return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
end
local scheme, host, port, path, query = unpack(httpc.parse_uri(nil, nomad_addr))
local ok, err = balancer.set_current_peer(host, port)
if not ok then
    ngx.log(ngx.ERR, "failed to set the current peer: ", err)
    return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
end
