--
local http = require("resty.http")
local cjson = require("cjson")

---@alias Httpc any

---@class M
---@field _conn Httpc?
local M = {}
M.__index = M

---@return M
function M.new()
    return setmetatable({A = 1}, M)
end

---@return Httpc
function M:httpc()
    if self._httpc == nil then
        self._httpc = http.new()
    end
    return self._httpc
end

---@param status integer
---@param message string
---@return integer
function M:exit(status, message)
    ngx.status = status
    ngx.say(message)
    ngx.exit(status)
    return status
end

---@param message string
---@return integer
function M:exit_forbid(message)
    return M:exit(ngx.HTTP_FORBIDDEN, message)
end

---@param message string
---@return integer
function M:exit_internal(message)
    return M:exit(ngx.HTTP_INTERNAL_SERVER_ERROR, message)
end

---@param fmt string
function M:err(fmt, ...)
    ngx.log(ngx.ERR, fmt:format(...))
end

---@return string?
function M:_get_token_key()
    local h, err = ngx.req.get_headers()
    if err then
        self:exit_internal("error getting headers: " .. tostring(err))
    end
    for k, v in pairs(h) do
        if k:lower() == "x-nomad-token" then
            if type(v) == "table" then
                return v[0]
            else
                return v
            end
        end
    end
    return nil
end

---@param url string
---@return any?
function M:api_get(url)
    local nomad_addr = assert(os.getenv("NOMAD_ADDR"))
    local uri = nomad_addr .. ("/v1/" .. url):gsub("//", "/")
    local res, err =
        self:httpc():request_uri(
        uri,
        {headers = {["Content-Type"] = "application/json", ["X-Nomad-Token"] = self:_get_token_key()}}
    )
    if not res then
        self:err("request to %s failed: %s", uri, tostring(err))
        return nil
    end
    local inspect = require("resty.inspect")
    if res.status ~= ngx.HTTP_OK then
        self:err("request to %s failed with status %s", uri, res.status)
        return nil
    end
    return cjson.decode(res.body)
end

---@class Token
---@field AccessorID string
---@field Type string
---@field Name string

-- Returns TOKEN id that the request was made with.
---@return Token?
function M:get_token()
    local token = self:_get_token_key()
    if not token then
        return nil
    end
    return self:api_get("/acl/token/self")
end

---@return any?
function M:get_job()
    ngx.req.read_body()
    local data = ngx.req.get_body_data()
    if data == nil then
        return nil
    end
    local job = cjson.decode(data).Job
    local args = ngx.req.get_uri_args()
    job.Namespace = job.Namespace or args.namespace
    job.Region = job.Region or args.Region
    return job
end

return M
