MAKEFLAGS = -rR
up:
	docker compose up --build
reload:
	docker compose exec nomad-proxy sh -xc 'nginx -t && nginx -s reload'
shell:
	docker compose run --entrypoint= nomad-proxy bash -l

