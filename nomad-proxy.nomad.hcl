locals {
  dir = abspath(".")
}
job "nomad-proxy" {
  group "nomad-proxy" {
    network {
      port "nomad" {
        static = 8686
      }
    }
    task "nomad-proxy" {
      driver = "docker"
      config {
        image = "openresty/openresty:1.21.4.1-0-alpine-apk"
        ports = ["nomad"]
        mount {
          type = "bind"
          source = "${local.dir}/conf.d"
          target = "/etc/nginx/conf.d"
          readonly = true
        }
        mount {
          type = "bind"
          source = "${local.dir}/conf.d"
          target = "/etc/nginx/conf.d"
          readonly = true
        }
      }
    }
  }
}
