FROM openresty/openresty:alpine-fat
RUN opm get \
	ledgetech/lua-resty-http \
	xiangnanscu/lua-resty-inspect
COPY ./lua /lua/
COPY ./nginx.conf /usr/local/openresty/nginx/conf/nginx.conf
COPY ./nginx.d/* /etc/nginx/conf.d/
